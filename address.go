package GoWinDivert

/*
#include <windows.h>

// WinDivert layers.
typedef enum
{
WINDIVERT_LAYER_NETWORK = 0,        // Network layer.
WINDIVERT_LAYER_NETWORK_FORWARD = 1,// Network layer (forwarded packets)
WINDIVERT_LAYER_FLOW = 2,           // Flow layer.
WINDIVERT_LAYER_SOCKET = 3,         // Socket layer.
WINDIVERT_LAYER_REFLECT = 4,        // Reflect layer.
} WINDIVERT_LAYER, *PWINDIVERT_LAYER;
// WinDivert NETWORK and NETWORK_FORWARD layer data.
typedef struct
{
UINT32 IfIdx;                       //Packet's interface index.
UINT32 SubIfIdx;                    // Packet's sub-interface index.
} WINDIVERT_DATA_NETWORK, *PWINDIVERT_DATA_NETWORK;

// WinDivert FLOW layer data.
typedef struct
{
UINT64 EndpointId;                  // Endpoint ID.
UINT64 ParentEndpointId;            // Parent endpoint ID.
UINT32 ProcessId;                   // Process ID.
UINT32 LocalAddr[4];                // Local address.
UINT32 RemoteAddr[4];               // Remote address.
UINT16 LocalPort;                   // Local port.
UINT16 RemotePort;                  // Remote port.
UINT8  Protocol;                    // Protocol.
} WINDIVERT_DATA_FLOW, *PWINDIVERT_DATA_FLOW;

// WinDivert SOCKET layer data.
typedef struct
{
UINT64 EndpointId;                  // Endpoint ID.
UINT64 ParentEndpointId;            // Parent Endpoint ID.
UINT32 ProcessId;                   // Process ID.
UINT32 LocalAddr[4];                // Local address.
UINT32 RemoteAddr[4];               // Remote address.
UINT16 LocalPort;                   // Local port.
UINT16 RemotePort;                  // Remote port.
UINT8  Protocol;                    // Protocol.
} WINDIVERT_DATA_SOCKET, *PWINDIVERT_DATA_SOCKET;

// WinDivert REFLECTION layer data.
typedef struct
{
INT64  Timestamp;                   // Handle open time.
UINT32 ProcessId;                   // Handle process ID.
WINDIVERT_LAYER Layer;              // Handle layer.
UINT64 Flags;                       // Handle flags.
INT16  Priority;                    // Handle priority.
} WINDIVERT_DATA_REFLECT, *PWINDIVERT_DATA_REFLECT;

// WinDivert address.
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4201)
#endif
typedef struct
{
INT64  Timestamp;                   // Packet's timestamp.
UINT32 Layer:8;                     // Packet's layer.
UINT32 Event:8;                     // Packet event.
UINT32 Sniffed:1;                   // Packet was sniffed?
UINT32 Outbound:1;                  // Packet is outound?
UINT32 Loopback:1;                  // Packet is loopback?
UINT32 Impostor:1;                  // Packet is impostor?
UINT32 IPv6:1;                      // Packet is IPv6?
UINT32 IPChecksum:1;                // Packet has valid IPv4 checksum?
UINT32 TCPChecksum:1;               // Packet has valid TCP checksum?
UINT32 UDPChecksum:1;               // Packet has valid UDP checksum?
UINT32 Reserved1:8;
UINT32 Reserved2;
union
{
WINDIVERT_DATA_NETWORK Network; // Network layer data.
WINDIVERT_DATA_FLOW Flow;       // Flow layer data.
WINDIVERT_DATA_SOCKET Socket;   // Socket layer data.
WINDIVERT_DATA_REFLECT Reflect; // Reflect layer data.
UINT8 Reserved3[64];
};
} WINDIVERT_ADDRESS, *PWINDIVERT_ADDRESS;
#ifdef _MSC_VER
#pragma warning(pop)
#endif

void setNetwork(WINDIVERT_ADDRESS* winDivertAddress,UINT32 ifIdx,UINT32 subIfIdx){
	winDivertAddress->Network.IfIdx = ifIdx;
	winDivertAddress->Network.SubIfIdx = subIfIdx;
}

void setSniffed(WINDIVERT_ADDRESS* winDivertAddress,UINT32 sniffed){
	winDivertAddress->Sniffed = sniffed;
}

void setOutbound(WINDIVERT_ADDRESS* winDivertAddress,UINT32 direction){
	winDivertAddress->Outbound = direction;
}

void setLoopback(WINDIVERT_ADDRESS* winDivertAddress,UINT32 loopback){
	winDivertAddress->Loopback = loopback;
}

void setImpostor(WINDIVERT_ADDRESS* winDivertAddress,UINT32 impostor){
	winDivertAddress->Impostor = impostor;
}

void setIPv6(WINDIVERT_ADDRESS* winDivertAddress,UINT32 ipv6){
	winDivertAddress->IPv6 = ipv6;
}
*/
import "C"

type WinDivertAddress C.WINDIVERT_ADDRESS

func (winDivertAddress *WinDivertAddress) SetNetwork(ifIdx uint32, subIfIdx uint32) {
	C.setNetwork((*C.WINDIVERT_ADDRESS)(winDivertAddress), C.uint(ifIdx), C.uint(subIfIdx))
}

func (winDivertAddress *WinDivertAddress) SetSniffed(sniffed uint) {
	C.setSniffed((*C.WINDIVERT_ADDRESS)(winDivertAddress), C.uint(sniffed))
}

func (winDivertAddress *WinDivertAddress) SetOutbound(direction uint) {
	C.setOutbound((*C.WINDIVERT_ADDRESS)(winDivertAddress), C.uint(direction))
}
func (winDivertAddress *WinDivertAddress) SetLoopback(loopback uint) {
	C.setLoopback((*C.WINDIVERT_ADDRESS)(winDivertAddress), C.uint(loopback))
}
func (winDivertAddress *WinDivertAddress) SetImpostor(impostor uint) {
	C.setImpostor((*C.WINDIVERT_ADDRESS)(winDivertAddress), C.uint(impostor))
}

func (winDivertAddress *WinDivertAddress) SetIPv6(ipv6 uint) {
	C.setIPv6((*C.WINDIVERT_ADDRESS)(winDivertAddress), C.uint(ipv6))
}
